
let randomArray;

function getRandomNumber(min, max) {
    let rand = min -0.5 + Math.random() * (max - min + 1);
    return Math.round(rand);
}


//---------------------------------------------------------------------Random Array
console.log('Рандомный массив');

function generateRandomArray() {
    let array = [];
    for (let i=0; i < 10; i++) {
        let number = getRandomNumber(1, 100);
        array.push(number);
    }
    return array;
}

randomArray = generateRandomArray() ;

console.log(randomArray);

//---------------------------------------------------------------------AVG

console.log('Среднее арифметическое рандомного массива');

function getAverage() {
    let summ = 0;
    for(let i = 0; i < randomArray.length; i++) {
        summ += randomArray[i];
    }
    let avg = summ / randomArray.length;
    return avg;
}

let avg = getAverage();
console.log(avg);


//---------------------------------------------------------------------AVG geometric
function getGeometricMean() {
    let multyplay = 1;
    for(let i = 0; i < randomArray.length; i++) {
        multyplay = multyplay * randomArray[i];
    }
    let geometricMean = Math.pow(multyplay, (1 / randomArray.length) );
    return geometricMean;
}

let geometricMean = getGeometricMean();
console.log('Среднее геометрическое рандомного массива 1 способ');
console.log(geometricMean);


function getGeometricMean2() {
    let multyplay = 1;
    randomArray.forEach(item => multyplay = multyplay * item);
    let geometricMean = Math.pow(multyplay, (1 / randomArray.length) );
    return geometricMean;
}

let geometricMean2 = getGeometricMean2();
console.log('Среднее геометрическое рандомного массива 2 способ');
console.log(geometricMean2);


function getGeometricMean3(array) {
    let multyplay = 1;
    array.forEach(item => multyplay = multyplay * item);
    let geometricMean = Math.pow(multyplay, (1 / array) );
    return geometricMean;
}

let geometricMean3 = getGeometricMean2(randomArray);
console.log('Среднее геометрическое рандомного массива 3 способ, для любого массива');
console.log(geometricMean3);


//-----------------------------------------------------------------------------------------массив случайных чисел


const NAMES = ['Иван', 'Олег', 'Николай', 'Игорь', 'Елена', 'Андрей', 'Артур', 'Павел', 'Борис', 'Сергей', 'Виктор', 'Виталий', 'Галина Петровна', 'Глеб', 'Рамзан', 
'Диана', 'Дарья', 'Жанибек', 'Илдырым', 'Нурсултан'];

function getRandomNames() {
    
    let names = [];
    for (let i = 0; i < 10; i++) {
        let index = getRandomNumber(0, NAMES.length-1)
        names.push(NAMES[index]);
    }

    return names;
}

let names = getRandomNames();
console.log(names);


//-----------------------------------------------------------------------------------------sorting
// .sort((a, b) => { -1 0 1})

randomArray.sort((a, b) => b - a);
console.log('Сортировка для чисел');
console.log(randomArray);


// sorting strings
names.sort((a, b) => a.localeCompare(b));
console.log('Сортировка для строк');
console.log(names);

/*
function generateRandomUsers() {

    for (let i = 0; i<5; i++) {
        let user = {
            name: '',
            age: getRandomNumber (10, 100)
        };
        users.push(user);
    }
    
}  */

let users = [];

function GenerateID() {
    let rand = getRandomNumber(0, 100);
    
    return id;
}

names.index.forEach()

function GenerateRandomUsers() {

    for (let i = 0; i<10; i++) {
        
        let index = getRandomNumber(0, NAMES.length-1)
        let user = {
            name: NAMES[index],
            age: getRandomNumber (10, 100)
        };
        users.push(user);
    }
    
}



GenerateRandomUsers();
console.log(users);